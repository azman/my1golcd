/**
*
* wxcanvas.cpp
*
* - implementation for wx-based drawing canvas
*
**/

#include "wxcanvas.hpp"

my1Canvas::my1Canvas(wxWindow *parent)
	: wxPanel(parent)
{
	// parameters
	mImageWidth = IMAGE_WIDTH_DEFAULT; mImageHeight = IMAGE_HEIGHT_DEFAULT;
	mImageGridSize = IMAGE_GRIDSIZE_DEFAULT; mImageBankSize = IMAGE_BANKSIZE_DEFAULT;
	mImageChanged = false; mParent = (wxFrame*) parent;

	// wxobjects
	mGridImage = new wxBitmap(mImageWidth*mImageGridSize, mImageHeight*mImageGridSize);
	mCurrentImage = new wxImage(mImageWidth, mImageHeight);
	ResetImage();
	RedrawGrid();
	//RedrawMain();

	// gui events
	this->Connect(wxEVT_PAINT, wxPaintEventHandler(my1Canvas::OnPaint));
	this->Connect(wxEVT_LEFT_DOWN, wxMouseEventHandler(my1Canvas::OnMouseClick));
	this->Connect(wxEVT_RIGHT_DOWN, wxMouseEventHandler(my1Canvas::OnMouseClick));
	this->Connect(wxEVT_MOTION, wxMouseEventHandler(my1Canvas::OnMouseMove));
	this->Connect(wxEVT_LEAVE_WINDOW, wxMouseEventHandler(my1Canvas::OnMouseLeave));
}

void my1Canvas::RedrawGrid(void)
{
	// recreate grid image
	mGridImage->Create(mImageWidth*mImageGridSize, mImageHeight*mImageGridSize);
	// prepare grid canvas
	wxMemoryDC dc;
	dc.SelectObject(*mGridImage);
	dc.SetBackground(*wxWHITE_BRUSH);
	dc.Clear();
	//dc.SetPen(*wxRED_PEN);
	//dc.SetBrush(*wxRED_BRUSH);

	// draw the grid
	int cMainGrid = 0;
	int cHeight = mImageHeight*mImageGridSize;
	int cWidth = mImageWidth*mImageGridSize;
	// draw row lines
	for(int cLoop=mImageGridSize-1;cLoop<cHeight-1;cLoop+=mImageGridSize)
	{
		cMainGrid++;
		if(cMainGrid%mImageBankSize==0)
			dc.SetPen(wxPen(wxColour(128,128,128), 1, wxSOLID));
		else
			dc.SetPen(wxPen(wxColour(192,192,192), 1, wxSOLID));
		dc.DrawLine(0, cLoop, cWidth-2, cLoop);
	}
	// draw col lines
	dc.SetPen(wxPen(wxColour(192,192,192), 1, wxSOLID));
	for(int cLoop=mImageGridSize-1;cLoop<cWidth-1;cLoop+=mImageGridSize)
	{
		dc.DrawLine(cLoop, 0, cLoop, cHeight-2);
	}

	// release draw objects
	dc.SetPen(wxNullPen);
	dc.SelectObject(wxNullBitmap);
}

void my1Canvas::RedrawMain(void)
{
	// prepare grid canvas
	wxMemoryDC dc;
	dc.SelectObject(*mGridImage);
	dc.SetPen(wxPen(wxColour(0,0,0), 1, wxSOLID));
	// draw from image object?
	unsigned char *pPixel = mCurrentImage->GetData();
	for(int cRow=0;cRow<mImageHeight;cRow++)
	{
		for(int cCol=0;cCol<mImageWidth;cCol++)
		{
			// check bit and set color
			long index = ((cRow * mImageWidth + cCol) * 3);
			if(pPixel[index]==PIXEL_GRAY_BLACK)
			{
				int iRow = cRow * mImageGridSize;
				int iCol = cCol * mImageGridSize;
				for(int cLoop=0;cLoop<mImageGridSize-1;cLoop++)
				{
					for(int cLoop1=0;cLoop1<mImageGridSize-1;cLoop1++)
					{
						dc.DrawPoint(cLoop1+iCol,cLoop+iRow);
					}
				}
			}
		}
	}
	// release draw objects
	dc.SetPen(wxNullPen);
	dc.SelectObject(wxNullBitmap);
}

void my1Canvas::ResetImage(void)
{
	// prepare temporary bitmap for drawing
	wxBitmap tmpImage(mImageWidth, mImageHeight);
	// draw on temporary canvas and converto to image
	wxMemoryDC dc;
	dc.SelectObject(tmpImage);
	dc.SetBackground(*wxWHITE_BRUSH);
	dc.Clear();
	*mCurrentImage = tmpImage.ConvertToImage();
	// release draw object
	dc.SelectObject(wxNullBitmap);
}

void my1Canvas::RescaleImage(void)
{
	// just rescale image 
	mCurrentImage->Rescale(mImageWidth,mImageHeight);
}

void my1Canvas::OnPaint(wxPaintEvent& event)
{
	// prepare device context
	wxPaintDC pdc(this);
	wxDC &dc = pdc;
	PrepareDC(dc);
	dc.SetBackgroundMode(wxSOLID);  // wxTRANSPARENT @ wxSOLID;
	dc.SetBackground(wxBrush(wxColour(255,255,255),wxSOLID));
	dc.SetTextForeground(wxColour(0,0,0));
	dc.SetTextBackground(wxColour(255,255,255));
	//dc.Clear();
	//dc.SetPen(wxPen(wxColour(0,0,0), 1, wxSOLID));

	// blit (overlay?) the grid image (faster!)
	wxMemoryDC temp_dc;
	temp_dc.SelectObject(*mGridImage);
	dc.Blit(0,0,mGridImage->GetWidth(),mGridImage->GetHeight(),&temp_dc,0,0);
	temp_dc.SelectObject(wxNullBitmap);

	//dc.SetPen(wxNullPen);
}

void my1Canvas::OnMouseClick(wxMouseEvent &event)
{
	// get event location
	wxPoint pos = event.GetPosition();
	int cCoordX = pos.x/mImageGridSize;
	int cCoordY = pos.y/mImageGridSize;
	unsigned char *pPixel = mCurrentImage->GetData();
	long index = ((cCoordY * mImageWidth + cCoordX) * 3);

	// set pixel status & update image object as well?
	wxColour pcolor;
	if(event.LeftDown())
	{
		pcolor = wxColour(0,0,0);
		pPixel[index] = pPixel[index+1] = pPixel[index+2] = PIXEL_GRAY_BLACK;
	}
	else if(event.RightDown())
	{
		pcolor = wxColour(255,255,255);
		pPixel[index] = pPixel[index+1] = pPixel[index+2] = PIXEL_GRAY_WHITE;
	}

	// update display
	wxMemoryDC dc;
	dc.SelectObject(*mGridImage);
	dc.SetPen(wxPen(pcolor, 1, wxSOLID));
	cCoordX *= mImageGridSize;
	cCoordY *= mImageGridSize;
	for(int cLoop=0;cLoop<mImageGridSize-1;cLoop++)
	{
		for(int cLoop1=0;cLoop1<mImageGridSize-1;cLoop1++)
		{
			dc.DrawPoint(cLoop1+cCoordX,cLoop+cCoordY);
		}
	}
	mImageChanged = true;
	dc.SetPen(wxNullPen);
	dc.SelectObject(wxNullBitmap);
	this->Refresh();
}

void my1Canvas::OnMouseMove(wxMouseEvent &event)
{
	// get event location
	wxPoint pos = event.GetPosition();
	int posX = (int)(pos.x/mImageGridSize);
	int posY = (int)(pos.y/mImageGridSize);

	// convert to string
	wxString str;
	str.Printf(wxT("Pixel position: %d,%d"), posX, posY);

	// show on parent's status bar
	mParent->SetStatusText(str, 1);
}

void my1Canvas::OnMouseLeave(wxMouseEvent &event)
{
	// show on parent's status bar
	mParent->SetStatusText(wxT(""), 1);
}

