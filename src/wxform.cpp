/**
*
* wxform.cpp
*
* - implementation for main wx-based form
*
**/

#include "wxform.hpp"
#include <wx/textfile.h>
#include <wx/image.h>

#if USE_XPM_BITMAPS
	#define MY1BITMAP_EXIT   "res/exit.xpm"
	#define MY1BITMAP_NEW    "res/new.xpm"
	#define MY1BITMAP_OPEN   "res/open.xpm"
	#define MY1BITMAP_SAVE   "res/save.xpm"
	#define MY1BITMAP_BINARY "res/binary.xpm"
	#define MY1BITMAP_OPTION "res/option.xpm"
#else
	#define MY1BITMAP_EXIT   "exit"
	#define MY1BITMAP_NEW    "new"
	#define MY1BITMAP_OPEN   "open"
	#define MY1BITMAP_SAVE   "save"
	#define MY1BITMAP_BINARY "binary"
	#define MY1BITMAP_OPTION "option"
#endif

my1Form::my1Form(const wxString &title)
	: wxFrame( NULL, wxID_ANY, title, wxDefaultPosition, wxSize(320, 240), wxDEFAULT_FRAME_STYLE & ~ (wxRESIZE_BORDER | wxMAXIMIZE_BOX) )
{
	wxInitAllImageHandlers();
	SetIcon(wxIcon(wxT(MY1APP_ICON)));
	wxIcon mIconExit(wxT(MY1BITMAP_EXIT));
	wxIcon mIconClear(wxT(MY1BITMAP_NEW));
	wxIcon mIconLoad(wxT(MY1BITMAP_OPEN));
	wxIcon mIconSave(wxT(MY1BITMAP_SAVE));
	wxIcon mIconGenerate(wxT(MY1BITMAP_BINARY));
	wxIcon mIconOptions(wxT(MY1BITMAP_OPTION));

	wxToolBar *mainTool = CreateToolBar();
	// our icon is 16x16, windows defaults to 24x24
	//mainTool->SetToolBitmapSize(wxSize(16,16));
	mainTool->AddTool(MY1ID_EXIT, wxT("Exit"), mIconExit);
	mainTool->AddSeparator();
	mainTool->AddTool(MY1ID_CLEAR, wxT("Clear"), mIconClear);
	mainTool->AddTool(MY1ID_LOAD, wxT("Load"), mIconLoad);
	mainTool->AddTool(MY1ID_SAVE, wxT("Save"), mIconSave);
	mainTool->AddSeparator();
	mainTool->AddTool(MY1ID_GENERATE, wxT("Generate"), mIconGenerate);
	mainTool->AddTool(MY1ID_OPTIONS, wxT("Options"), mIconOptions);
	mainTool->Realize();

	CreateStatusBar(2);
	SetStatusText(_T("Welcome to my1GoLCD!"));

	mCanvas = new my1Canvas(this);
	SetClientSize(mCanvas->mImageWidth*mCanvas->mImageGridSize,
		mCanvas->mImageHeight*mCanvas->mImageGridSize);
	// SendSizeEvent(); // just in case??

	mOption.mChanged = 0;
	mOption.mFlag = 0;

	// actions!
	this->Connect(MY1ID_EXIT, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(my1Form::OnQuit));
	this->Connect(MY1ID_CLEAR, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(my1Form::OnClear));
	this->Connect(MY1ID_LOAD, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(my1Form::OnLoad));
	this->Connect(MY1ID_SAVE, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(my1Form::OnSave));
	this->Connect(MY1ID_GENERATE, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(my1Form::OnGenerate));
	this->Connect(MY1ID_OPTIONS, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(my1Form::OnCheckOptions));

	Centre();
}

void my1Form::OnQuit(wxCommandEvent& event)
{
	Close(true);
}

void my1Form::OnClear(wxCommandEvent& event)
{
	mCanvas->ResetImage();
	mCanvas->Refresh();
	mCanvas->mImageChanged = false;
}

void my1Form::OnLoad(wxCommandEvent& event)
{
	if(mCanvas->mImageChanged)
	{
		if(wxMessageBox(wxT("Current content has not been saved! Proceed?"), wxT("Please confirm"), wxICON_QUESTION | wxYES_NO, this) == wxNO)
			return;
	}

	wxString cFileName = wxFileSelector(_T("Select image file"));
	if(!cFileName)
		return;

	wxImage cTestImage;
	if( !cTestImage.LoadFile(cFileName) )
	{
		wxLogError(_T("Couldn't load image from '%s'."), cFileName.c_str());
		return;
	}

	while(cTestImage.GetWidth()!=mCanvas->mImageWidth||
			cTestImage.GetHeight()!=mCanvas->mImageHeight)
	{
		if(wxMessageBox(wxT("Try to rescale image to fit?"), wxT("Incompatible Image Size"), wxICON_QUESTION | wxYES_NO, this) == wxNO)
		{
			wxLogError(_T("Incompatible image size '%d x %d' (%dx%d)."), cTestImage.GetWidth(), cTestImage.GetHeight(), mCanvas->mImageWidth, mCanvas->mImageHeight);
			return;
		}
		cTestImage.Rescale(mCanvas->mImageWidth, mCanvas->mImageHeight);
	}

	cTestImage.ConvertToMono(255,255,255);
	mCanvas->mCurrentImage->Paste(cTestImage,0,0);
	mCanvas->RedrawGrid();
	mCanvas->RedrawMain();
	mCanvas->Refresh();
}

void my1Form::OnSave(wxCommandEvent& event)
{
	wxFileDialog saveFileDialog(this, wxT("Save image file"), wxT(""), wxT(""), wxT("BMP (*.bmp)|*.bmp"), wxFD_SAVE|wxFD_OVERWRITE_PROMPT);

	if(saveFileDialog.ShowModal() == wxID_CANCEL)
		return;

	mCanvas->mCurrentImage->SaveFile(saveFileDialog.GetPath(),wxBITMAP_TYPE_BMP);
	mCanvas->mImageChanged = false;
}

#define MASK_BIG_ENDIAN 0x80
#define MASK_LITTLE_ENDIAN 0x01

void my1Form::OnGenerate(wxCommandEvent& event)
{
	wxArrayString cList;
	wxString cBuffer, cConvert;
	bool cUseASM = false;
	wxFileDialog genFileDialog(this,wxT("Generated Filename"),wxT(""),wxT(""),
		wxT("C/C++ File (*.c;*.cpp)|*.c;*.cpp|ASM File (*.asm)|*.asm"),
		wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
	if(genFileDialog.ShowModal()==wxID_OK)
	{
		if(genFileDialog.GetFilterIndex()==1)
		{
			cBuffer = wxT("\tdfb "); // line start
			cUseASM = true;
		}
		else
		{
			cBuffer = wxT("\t"); // line start
			cList.Add(wxT("unsigned char cData[]={"));
		}
		unsigned char *pPixel = mCanvas->mCurrentImage->GetData();
		if (mOption.mFlag&MY1OPTIONS_FLAG_HPATTERN)
		{
			// horizontal pattern lcd data (bank ignored)
			if(cUseASM)
				cConvert.Printf(wxT("; horizontal pattern lcd data"));
			else
				cConvert.Printf(wxT("/* horizontal pattern lcd data */"));
			cList.Add(cConvert);
			for (int cLoop=0;cLoop<mCanvas->mImageHeight;cLoop++)
			{
				int cLoop1 = 0, cLoop2 = cLoop*mCanvas->mImageWidth, cInit = 0;
				unsigned char cByte, cEnds, cMask;
				cByte = 0;
				cEnds = mOption.mFlag&MY1OPTIONS_FLAG_BIGENDIAN?
					MASK_BIG_ENDIAN:MASK_LITTLE_ENDIAN;
				cMask = cEnds;
				while (cLoop1<mCanvas->mImageWidth)
				{
					if(pPixel[(cLoop2+cLoop1)*3]==PIXEL_GRAY_BLACK)
						cByte |= cMask;
					cLoop1++;
					if (cEnds==MASK_BIG_ENDIAN) cMask >>=1;
					else cMask <<=1;
					if (!(cMask&0xff))
					{
						cMask = cEnds;
						if (cInit) cBuffer << wxT(", ");
						if(cUseASM)
						{
							if (cByte>0x9f) cConvert.Printf(wxT("%03Xh"),cByte);
							else cConvert.Printf(wxT("%02Xh"),cByte);
						}
						else cConvert.Printf(wxT("0x%02X"),cByte);
						cByte = 0;
						cBuffer << cConvert;
						cInit++;
					}
				}
				if(!cUseASM) cBuffer << wxT(",");
				cList.Add(cBuffer);
				if(cUseASM) cBuffer = wxT("\tdfb ");
				else cBuffer = wxT("\t");
			}
		}
		else
		{
			// vertical pattern lcd data (process by banks)
			int cCount = 0;
			for(int cLoop=0;cLoop<mCanvas->mImageBankSize;cLoop++)
			{
				if(cUseASM)
					cConvert.Printf(wxT("; Start Bank %d"),cLoop);
				else
					cConvert.Printf(wxT("/* Start Bank %d */"),cLoop);
				cList.Add(cConvert);
				for(int cLoop1=0;cLoop1<mCanvas->mImageWidth;cLoop1++)
				{
					int cY = cLoop*8;
					unsigned char cByte = 0x00, cMask = 0x01;
					for(int cLoop2=0;cLoop2<8;cLoop2++)
					{
						if(pPixel[((cY+cLoop2) * mCanvas->mImageWidth + cLoop1) * 3]==PIXEL_GRAY_BLACK)
							cByte |= cMask;
						cMask <<=1;
					}
					if(cUseASM)
					{
						if (cByte>0x9f) cConvert.Printf(wxT("%03Xh"),cByte);
						else cConvert.Printf(wxT("%02Xh"),cByte);
					}
					else cConvert.Printf(wxT("0x%02X"),cByte);
					cBuffer << cConvert;
					cCount++;
					if(cCount==mCanvas->mImageBankSize)
					{
						if(!cUseASM) cBuffer << wxT(",");
						cList.Add(cBuffer);
						if(cUseASM) cBuffer = wxT("\tdfb ");
						else cBuffer = wxT("\t");
						cCount = 0;
					}
					else cBuffer << wxT(", ");
				}
			}
		}
		if(!cUseASM)
		{
			cBuffer = cList[cList.GetCount()-1];
			cList.RemoveAt(cList.GetCount()-1);
			cBuffer.RemoveLast(); // remove trailing comma
			cList.Add(cBuffer);
			cList.Add(wxT("};"));
		}
		wxTextFile genFile(genFileDialog.GetPath());
		if(genFile.Exists())
		{
			genFile.Open(genFileDialog.GetPath());
			genFile.Clear();
		}
		else
		{
			genFile.Create(genFileDialog.GetPath());
		}
		for(int cIndex=0;cIndex<(int)(cList.GetCount());cIndex++)
		{
			genFile.AddLine(cList[cIndex]);
		}
		genFile.Write();
		genFile.Close();
		cBuffer.Printf(wxT("LCD Data Table Created in %s."),genFileDialog.GetPath().c_str());
		wxMessageBox(cBuffer,wxT("Just FYI"),wxOK|wxICON_INFORMATION,this);
	}
}

void my1Form::OnCheckOptions(wxCommandEvent &event)
{
	my1Options cTemp = { mOption.mChanged, mOption.mFlag,
			mCanvas->mImageWidth, mCanvas->mImageHeight,
			mCanvas->mImageGridSize, mCanvas->mImageBankSize };
	my1OptionDialog *prefDialog = new my1OptionDialog(this, wxT("Options"), cTemp);
	prefDialog->ShowModal();
	prefDialog->Destroy();
	mOption = cTemp;
	if(mOption.mChanged)
	{
		mOption.mChanged = 0;
		mCanvas->mImageWidth = mOption.mWidth;
		mCanvas->mImageHeight = mOption.mHeight;
		mCanvas->mImageGridSize = mOption.mGridSize;
		mCanvas->mImageBankSize = mOption.mBankSize;
		//wxMessageBox(wxT("Changing Display!"),wxT("Just FYI"),wxOK|wxICON_INFORMATION,this);

		//Freeze();
		SetClientSize(mCanvas->mImageWidth*mCanvas->mImageGridSize,
			mCanvas->mImageHeight*mCanvas->mImageGridSize);
		mCanvas->RedrawGrid();
		// try to rescale instead of clearing!
		mCanvas->RescaleImage();
		//mCanvas->ResetImage();
		SendSizeEvent();
		//Thaw();
		Refresh();
	}
}
