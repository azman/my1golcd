/**
*
* wxmain.hpp
*
* - header for main wx-based application
*
**/

#include <wx/wx.h>

class my1App : public wxApp
{
	virtual bool OnInit();
};
