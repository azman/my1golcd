/**
*
* wxcanvas.hpp
*
* - header for wx-based drawing canvas
*
**/

#include <wx/wx.h>

#ifndef __MY1CANVAS_HPP__
#define __MY1CANVAS_HPP__

#define IMAGE_WIDTH_DEFAULT 128
#define IMAGE_HEIGHT_DEFAULT 64
//#define IMAGE_WIDTH_DEFAULT 84
//#define IMAGE_HEIGHT_DEFAULT 48
#define IMAGE_BANKSIZE_DEFAULT 8
#define IMAGE_GRIDSIZE_DEFAULT 5

#define PIXEL_GRAY_WHITE 0xff
#define PIXEL_GRAY_BLACK 0x00

//#define RGB_RED 0.297
//#define RGB_GREEN 0.589
//#define RGB_BLUE 0.114

class my1Canvas : public wxPanel
{
public:
	my1Canvas(wxWindow *parent);
	void RedrawGrid(void);
	void RedrawMain(void);
	void ResetImage(void);
	void RescaleImage(void);
	void OnPaint(wxPaintEvent &event);
	void OnMouseClick(wxMouseEvent &event);
	void OnMouseMove(wxMouseEvent &event);
	void OnMouseLeave(wxMouseEvent &event);
	wxImage *mCurrentImage;
	wxBitmap *mGridImage;
	int mImageWidth, mImageHeight;
	int mImageGridSize, mImageBankSize;
	bool mImageChanged;
	wxFrame *mParent;
};

#endif

