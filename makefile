# makefile for my1golcd

PROJECT = my1golcd
GUISPRO = $(PROJECT)
GUISOBJ = wxpref.o wxcanvas.o wxform.o wxmain.o
EXTPATH = src

DELETE = rm -rf

ifeq ($(DO_MINGW),yes)
	GUISPRO = $(PROJECT).exe
	GUISOBJ += wxmain.res
	# cross-compiler settings
	XTOOL_DIR ?= /home/share/tool/xtool-mingw
	ifneq ("$(wildcard $(XTOOL_DIR)/bin/*-gcc)","")
		XTOOL_TARGET = $(XTOOL_DIR)
	else
		XTOOL_TARGET = /usr
	endif
	XTOOL_PREFIX ?= i686-w64-mingw32-
	CROSS_COMPILE = $(XTOOL_TARGET)/bin/$(XTOOL_PREFIX)
	CFLAGS += -static
	CFLAGS += -I$(XTOOL_DIR)/include -DDO_MINGW
	LFLAGS += -L$(XTOOL_DIR)/lib
	# below is to remove console at runtime
	LFLAGS += -Wl,-subsystem,windows
	OFLAGS +=
	# '-mthreads' is not playing nice with others - has to go!
	WX_CONF = $(XTOOL_DIR)/bin/wx-config
	WX_LIBS = $(shell $(WX_CONF) --libs | sed 's/-mthreads//g')
	WX_CXXFLAGS = $(shell $(WX_CONF) --cxxflags | sed 's/-mthreads//g')
else
	WX_LIBS = $(shell wx-config --libs stc,core,base)
	WX_CXXFLAGS = $(shell wx-config --cxxflags)
endif

CFLAGS += -I$(EXTPATH) -Wall
LFLAGS +=
OFLAGS +=

CC = $(CROSS_COMPILE)gcc
CPP = $(CROSS_COMPILE)g++
RES = $(CROSS_COMPILE)windres
debug: CFLAGS += -DMY1DEBUG

all: gui

gui: $(GUISPRO)

new: clean all

debug: new

$(GUISPRO): $(GUISOBJ)
	$(CPP) $(CFLAGS) -o $@ $+ $(LFLAGS) $(OFLAGS) $(WX_LIBS)

%.o: src/%.c src/%.h
	$(CC) $(CFLAGS) -c $<

%.o: src/%.c
	$(CC) $(CFLAGS) -c $<

%.o: src/%.cpp src/%.hpp
	$(CPP) $(CFLAGS) -c $<

%.o: src/%.cpp
	$(CPP) $(CFLAGS) -c $<

wx%.o: src/wx%.cpp src/wx%.hpp
	$(CPP) $(CFLAGS) $(WX_CXXFLAGS) -c $<

wx%.o: src/wx%.cpp
	$(CPP) $(CFLAGS) $(WX_CXXFLAGS) -c $<

%.res: src/%.rc
	$(RES) $< -O coff -o $@

%.o: $(EXTPATH)/%.c $(EXTPATH)/%.h
	$(CC) $(CFLAGS) -c $<

%.o: $(EXTPATH)/%.c
	$(CC) $(CFLAGS) -c $<

clean:
	-$(DELETE) $(GUISPRO) $(GUISOBJ) $(PROJECT).exe wxmain.res
